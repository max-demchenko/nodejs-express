// requires...
const path = require("path");
const fs = require("fs");
// constants...

const allowedExtentions = ["log", "txt", "json", "yaml", "xml", "js"];
const filesDir = "./files/";

const checkExtention = (filename) => {
  return allowedExtentions.includes(path.extname(filename).slice(1));
};

function createFile(req, res, next) {
  const fileName = req.body.filename;
  const content = req.body.content;
  const password = req.body?.password;
  const filePath = filesDir + fileName;

  if (Object.keys(req.body).length === 0) {
    return res.status(400).send({ message: "Bad request" });
  }

  console.log(req.body);

  if (!fileName) {
    return res
      .status(400)
      .send({ message: "Please specify 'filename' parameter" });
  }

  if (!content) {
    return res
      .status(400)
      .send({ message: "Please specify 'content' parameter" });
  }

  if (!checkExtention(fileName)) {
    return res.status(400).send({
      message: `${path.extname(fileName)} extension is not supported `,
    });
  }

  if (fs.existsSync(filePath)) {
    return res.status(400).send({ message: "File already exists" });
  }

  const file = {
    filename: fileName,
    content: content,
    extension: path.extname(fileName).slice(1),
    uploadedDate: new Date(),
    password: password,
  };

  fs.writeFile(filePath, JSON.stringify(file), (err) => {
    if (err) return next(err);
    res.status(200).send({ message: "File created successfully" });
  });
}

function getFiles(req, res, next) {
  fs.readdir(filesDir, (err, files) => {
    if (err) return next(err);
    res.status(200).send({
      message: "Success",
      files: files,
    });
  });
}

const getFile = (req, res, next) => {
  const fileName = req.params.filename;
  const password = req.query.password;
  const filePath = filesDir + fileName;

  if (fs.existsSync(filePath)) {
    return fs.readFile(filePath, (err, data) => {
      if (err) {
        return next(err);
      }

      const fileData = JSON.parse(data);
      if (fileData.password !== password && fileData.password) {
        return res.status(400).send({ message: "Wrong password" });
      }
      res.status(200).send({ message: "Success", ...fileData });
    });
  }

  res
    .status(400)
    .send({ message: `No file with "${fileName}" filename found` });
};

const editFile = (req, res, next) => {
  const fileName = req.params.filename;
  const newContent = req.body.content;
  const password = req.query.password;
  const filePath = filesDir + fileName;

  if (!newContent) {
    return res
      .status(400)
      .send({ message: "Please specify 'content' parameter" });
  }

  if (fs.existsSync(filePath)) {
    return fs.readFile(filePath, (err, data) => {
      if (err) {
        return next(err);
      }

      const fileData = JSON.parse(data);

      if (fileData.password !== password && fileData.password) {
        return res.status(400).send({ message: "Wrong password" });
      }

      const newFile = {
        filename: fileData.filename,
        content: newContent,
        extension: fileData.extension,
        uploadedDate: fileName.uploadedDate,
        password: fileData.password,
      };

      fs.writeFile(filePath, JSON.stringify(newFile), (err) => {
        if (err) return next(err);
      });

      res.status(200).send({ message: "File successfully updated" });
    });
  }

  res
    .status(400)
    .send({ message: `No file with "${fileName}" filename found` });
};

const deleteFile = (req, res, next) => {
  const fileName = req.params.filename;
  const password = req.query.password;
  const filePath = filesDir + fileName;

  if (fs.existsSync(filePath)) {
    return fs.readFile(filePath, (err, data) => {
      if (err) {
        return next(err);
      }

      const fileData = JSON.parse(data);

      if (fileData.password !== password && fileData.password) {
        return res.status(400).send({ message: "Wrong password" });
      }

      fs.unlink(filePath, (err) => {
        if (err) return next(err);
      });

      res.status(200).send({ message: "File successfully deleted" });
    });
  }

  res
    .status(400)
    .send({ message: `No file with "${fileName}" filename found` });
};

module.exports = {
  createFile,
  getFiles,
  getFile,
  editFile,
  deleteFile,
};
